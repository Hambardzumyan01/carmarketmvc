﻿using Microsoft.EntityFrameworkCore;

namespace CarMarket.Models
{
    public class CarContext : DbContext

    {
        public  DbSet<Car> Cars { get; set; }
        public DbSet<Order> orders { get; set; }
        public  CarContext(DbContextOptions<CarContext> options)
            :base(options)
        {
            Database.EnsureCreated();
        }
    }
}
