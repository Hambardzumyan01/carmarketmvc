﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarMarket.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public string User { get; set; } 
        public string ContactCar { get; set; } 

        public int CarId { get; set; }
        public Car Phone { get; set; }
    }
}
