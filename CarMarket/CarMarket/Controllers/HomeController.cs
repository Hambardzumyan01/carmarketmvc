﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CarMarket.Models;

namespace MobileStore.Controllers
{
    public class HomeController : Controller
    {
        CarContext db;
        public HomeController(CarContext context)
        {
            db = context;
        }
        public IActionResult Index()
        {
            return View(db.Cars.ToList());
        }
    }
}