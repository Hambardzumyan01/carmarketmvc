﻿using System.Linq;
using CarMarket.Models;

namespace CarMarket
{
    public static class SampleData
    {
        public static void Initialize (CarContext context)
        {
            if (!context.Cars.Any())
            {
                context.Cars.AddRange(
                    new Car
                    {
                        Name = "mercedes-benz s-class (c217)",
                        Company = "Mercedes-Benz",
                        Country = "Germania",
                        Price = 25000
                    },
                      new Car
                      {
                          Name = "Mercedes-AMG C190 GT)",
                          Company = "Mercedes-Benz",
                          Country = "Germania",
                          Price = 35000
                      },
                      new Car
                      {
                          Name = "Концепт-кар Mercedes-Benz F700)",
                          Company = "Mercedes-Benz",
                          Country = "Germania",
                          Price = 15000
                      },
                        new Car
                        {
                            Name = "Mercedes-Benz SLR McLaren)",
                            Company = "Mercedes-Benz",
                            Country = "Germania",
                            Price = 225000
                        }
                    );
                context.SaveChanges();
            }
        }
    }
}
